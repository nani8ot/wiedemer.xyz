+++ 
draft = false
date = 2020-09-13T07:18:46+02:00
title = "Messenger"
description = "Ein persönliche paar Messenger Tipps"
slug = "" 
tags = ["recommendation"]
categories = ["Empfehlungsecke"]
externalLink = ""
series = []
+++


## Bitte kein Facebook
Es ist inzwischen kein Geheimnis mehr, dass Facebook seine dutzenden(!) Milliarden Umsatz durch den Verkauf von Daten und damit angepasster Werbung verdient.
Deshalb ganz einfach: WhatsApp nur dann verwenden, wenn es wirklich nicht auch andere Wege gibt.

Mike Kuketz hat zu Messengern eine [schöne Artikel-Reihe](https://www.kuketz-blog.de/die-verrueckte-welt-der-messenger-messenger-teil1/) geschrieben und auf [PrivacyTools](https://www.privacytools.io/software/real-time-communication/) gibt es eine kleine aber feine Auswahl.

Hier noch meine Top 3 der Messenger:
1. Der Messenger [Element](https://element.io/) gefällt mir am besten, da er als dezentraler Messenger von vielen unabhängigen Anbietern getragen wird (ähnlich zu eMail). Unter anderem wird das zugrundeliegende Matrix-Protokoll seit kurzem von [den französischen Behörden](https://www.golem.de/news/statt-whatsapp-frankreich-wandert-in-die-matrix-1902-139167.html) genutzt und wird künftig auch von der [der deutschen Bundeswehr](https://www.heise.de/newsticker/meldung/Bundeswehr-setzt-kuenftig-auf-Matrix-als-Messenger-4719474.html) verwendet.
2. Meine zweite Empfehlung ist Signal, da es zwar auch OpenSource ist, aber leider ebenfalls zentral ist.
3. Als drittes empfehle ich Threema, da es auch OpenSource wird und wie bei Element keine Telefonnummer benötigt wird. Leider ist Threema wie die meisten Messenger zentral (nur ein Anbieter).

Ein paar Punkte zu zentralen und dezentralen Messengern findet sich auch auf [privacytools.io](https://www.privacytools.io/software/real-time-communication/#im).
