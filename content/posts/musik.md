+++ 
draft = false
date = 2020-09-14T13:50:51+02:00
title = "Musik ist suppi"
description = "gutes Zeugs."
slug = "" 
tags = ["music", "recommendation"]
categories = ["Empfehlungsecke"]
externalLink = ""
series = []
+++

Definitiv nicht vollständig, aber das wichtigste ist da.

# Artists
1. [TheFatRat](https://www.youtube.com/user/ThisIsTheFatRat)
2. [Teminite](https://www.youtube.com/user/TeminiteMusic)
3. [Billy Talents](https://www.youtube.com/user/billytalent)
- [Alan Walker](https://www.youtube.com/user/DjWalkzz)
- [Whales (previously SexWhales)](https://www.youtube.com/user/SexWhalesTV)
- [Virtual Riot](https://www.youtube.com/user/OfficialVirtualRiot)
- [Skrillex](https://www.youtube.com/user/TheOfficialSkrillex)
- [Desmeon](https://www.youtube.com/results?search_query=desmeon)
- [Tristam](https://www.youtube.com/results?search_query=tristam)

# Songs
- [Foxsky - Kirby Smash](https://www.youtube.com/watch?v=mMMRGJCFsQE)
- [TheFatRat & AleXa (알렉사) - Rule The World](https://www.youtube.com/watch?v=OJdG8wsU8cw)
- [Caravan Palace - Lone Digger](https://www.youtube.com/watch?v=puVMqOQPOt0&list=RDEM4q3Zdu2PJkGobhYlI7MjGw&index=27)
- [Alan Walker - Sing me to sleep](https://www.youtube.com/watch?v=2i2khp_npdE&list=RDEM4q3Zdu2PJkGobhYlI7MjGw&index=39)
- [Twenty One Pilots - Stressed Out (Tomsize Remix)](https://www.youtube.com/watch?v=0t2tjNqGyJI)
- [Cartoon - Why We Lose (feat. Coleman Trapp)](https://www.youtube.com/watch?v=zyXmsVwZqX4)
- [Desmeon - On That Day (feat. ElDiablo, Flint & Zadik)](https://www.youtube.com/watch?v=PUpS-ZrrZnU)
- [Desmeon - Undone (feat. Steklo)](https://www.youtube.com/watch?v=0E6KXgWuaHo)
- [Tristam - Till It's Over](https://www.youtube.com/watch?v=usXhiWE2Uc0)
- [Skrillex - Bangarang feat. Sirah](https://www.youtube.com/watch?v=YJVmu6yttiw)
- [Skrillex - First Of The Year (Equinox)](https://www.youtube.com/watch?v=2cXDgFwE13g)
- [Skrillex & Damian "Jr. Gong" Marley - Make It Bun Dem](https://www.youtube.com/watch?v=BGpzGu9Yp6Y)
- [Skrillex - Ruffneck - FULL Flex](https://www.youtube.com/watch?v=_t2TzJOyops)
- [Skrillex - Ease My Mind with Niki & The Dove](https://www.youtube.com/watch?v=hwsXo6fsmso)
- [Skrillex - Scary Monsters And Nice Sprites](https://www.youtube.com/watch?v=WSeNSzJ2-Jw)
- [Nightcore - Pretty Little Psycho](https://www.youtube.com/watch?v=BByMzI1YjKA)
- [EnV - 5000 Strong](https://www.youtube.com/watch?v=ghSgajaN7lk)
- [Teminite & PsoGnar - Lion's Den](https://www.youtube.com/watch?v=2-uZplR-Mbk)

# Playlists
- [NCS: Elevate](https://www.youtube.com/watch?v=Fmf-G9fpwto&list=PLRBp0Fe2GpgnRqNWGoCfvQ6NMoj59-Ybn)
- [NCS: Melodic Dubstep](https://www.youtube.com/watch?v=Qj2seyOEKG0)
- [NCS: Dubstep ](https://www.youtube.com/watch?v=Qj2seyOEKG0&list=PLRBp0Fe2Gpglq-J-Hv0p-y0wk3lQk570u)


# Raus mit euch
Hör ich nimmer so, habs aber mal, deswegen ists trotzdem hier.
## Artists
hmmm
## Songs
summm
