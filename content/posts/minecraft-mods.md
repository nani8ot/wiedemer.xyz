+++ 
draft = false
date = 2020-12-24T01:11:20+01:00
title = "Minecraft Modding"
description = "My favorite Minecraft mods and the tools I use."
slug = "" 
tags = ['minecraft', 'mods', 'modding', 'gaming']
categories = ['gaming']
externalLink = ""
series = []
+++

# Why should you?
1. Vanilla Minecraft -- or unmodified Minecraft -- is slow.
Java isn't exactly the best programming language to develop games,
but on top is there not enough optimising done by the Developers at Mojang/Microsoft.  
With some mods it is possible to gain between 30% and 300% of fps (frames per second). As an example, this could be the difference between barely playable (30fps) and quite good (60fps).
2. It is fun.  
It is just a great vibe to change the behaviour of game and improve the experience.

Following are the mods I use and/or like the most.  
They are worth looking into. My word.

# Launcher
The default Minecraft Launcher by Mojang is fine, but there are 3rd party clients which add useful features I don't want to miss anymore.  
My favourite client is [MultiMC](https://multimc.org/).
This client might not look as good as the default, but it packs many useful features for anyone who wants more and easier configuration.  
From the Project's website:

> -  Manage multiple instances of Minecraft at once
> -  Start Minecraft with a custom resolution
> -  Change Java's runtime options (including memory options)
> -  Shows Minecraft's console output in a colour coded window
> -  Kill Minecraft easily if it crashes / freezes
> -  Custom icons and groups for instances
> -  Forge integration (automatic installation, version downloads, mod management)
> -  Minecraft world management
> -  Import and export Minecraft instances to share them with anyone
> -  Supports every version of Minecraft that the vanilla launcher does

The best feature of this client is the isolation of profiles, so that mods won't interfere with other versions of the game (Good xD).  
There is also a one-click mod loader installation for [Fabric](https://fabricmc.net/) as well as [Forge](https://files.minecraftforge.net/).

# Mod Loader
There are mainly two mod loader -- mods which are used to load mods.  
[Forge](https://files.minecraftforge.net/) is around for more than a decade and thus has many mods, but it is slow and takes a long time (months) to be ported to newer versions of Minecraft.  
The advantages outweigh the downsides, at least for me, so Fabric is the mod loader of my choice (most of the time).

# Fabric Mod Collection
These mods are great and are worth looking into.  
Mods are only required on the client side, or else it is noted with **(Server)**.  
No particular order.
## Must have
Sodium, Phosphor, Lithium
: Great performance enhancements (fps: +30-300%)

Mod Menu
: Access your mods settings

Name Pain
: Show other players health in a non-intrusive way

Ok Zoomer
: Zoom... Nothing more, nothing less

Mouse Wheelie
: (Don't) Sort your inventory

Light Overlay
: Show where monsters could spawn. Ausleuchten!

Presence Footsteps
: Never heard the tall grass your walking through?

## Still great and daily driven
Xaero's Minimap & Worldmap
: Never walk in the dark

Roughly Enough Items & Resources
: Who needs Mincraft Wikis? Show Recipes

Hwyla
: Know what you are looking at. Must have in larger mod packs.

Craft Presence
: Display in Discord where you're playing

Chat Heads
: Players head next to chat messages

Blur
: Nice Blur in the Inventory

Shulker Box Tooltip
: You know what's inside, don't you?

LambdaBetterGrass
: It's better!

Illuminations
: Fireflies!

Falling Leaves
: You're not dumb

LambDynamicLights
: Who needs to place a torch?

## Sometimes...
Elytra Auto Flight
: You need to fly very, very long distance? Attention, your Elytra might break.

Autofish
: You never leveld that fast

