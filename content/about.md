---
title: "About"
date: 2020-09-13T07:15:41+02:00
draft: false
---

## And I am...
irgendwer (meine Name steht da glaub irgendwo...).

Und der Zweck dieser Seite ist es, meine kleine Seite aufzubauen, so dass man mich auch ganz ohne Facebook finden kann. Yay.



## Contact
- eMail: contact@wiedemer.xyz¹
- \[matrix\]: [@mousedr0id:lankolol.de](matrix:u/mousedr0id:lankolol.de)

¹Die eMailadresse ist nur zur Kontaktaufnahme, denn ich möchte wegen Spam nicht meine primäre eMail-Adresse und Telefonnummer herausgeben.

Zum Thema Messenger habe ich schon einen kleinen [Artikel](/posts/messenger/) geschrieben.

Kleiner Tipp: Mit [DeltaChat](https://delta.chat/de/) kann man ganz bequem alle Leute verschlüsselt per eMail erreichen. Damit muss dann gar nicht mit dem PGP-Key hier gearbeitet werden, sondern alles passiert automatisch im Hintergrund. 

## Some Details
**Name:**  
Aaron Wiedemer  
**Wohnort:**  
Umkreis Freiburg  
